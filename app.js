const express = require('express')
const path = require('path')
const { port } = require('./config')
const db = require('./models')
const logger = require('./log/logger')
const userRoute = require('./routes/getUsers')
const sendEmail = require('./routes/sendEmail')
const app = express()
app.use(express.json())
app.use(express.static(path.join(__dirname, 'client')))

app.use('/send', sendEmail)

app.use('/user', userRoute)
app.use((req, res) => {
  res.status(404)
  res
    .json({
      error: {
        message: 'Page Not Found',
      },
    })
    .end()
})

// db.sequelize
//   .sync()
//   .then(() => {
//     app.listen(port, () => {
//       logger.log('info', `Server Running on ${port}...`)
//     })
//   })
//   .catch((err) => {
//     logger.log('error', 'Unable to connect to the database:' + err)
//   })

app.listen(port, () => {
  logger.log('info', `Server Running on ${port}...`)
})
