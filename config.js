const dotenv = require('dotenv')
dotenv.config()

module.exports = {
  port: process.env.PORT,
  db_username: process.env.DB_USERNAME,
  db_password: process.env.DB_PASSWORD,
  db_host: process.env.DB_HOST,
  db_database: process.env.DB_DATABASE,
  user_email: process.env.USER_EMAIL,
  user_password: process.env.USER_PASSWORD,
  mail_api_key: process.env.MAIL_API,
  mail_domain: process.env.MAIL_DOMAIN,
}
