const express = require('express')
const logger = require('../log/logger')
const { mail_api_key, mail_domain } = require('../config')
const nodemailer = require('nodemailer')
const mailGun = require('nodemailer-mailgun-transport')

const route = express.Router()

route.post('/', (req, res) => {
  const output = `
       <h2>Memory Game</h2>
      <p>Hi ${req.body.name},<br> You have a new score: ${req.body.score} ;)</p>
    `

  const auth = {
    auth: {
      api_key: mail_api_key,
      domain: mail_domain,
    },
  }

  let transporter = nodemailer.createTransport(mailGun(auth))

  let mailOptions = {
    from: 'noreply@memory-game.com',
    to: req.body.email,
    subject: 'New Score',
    text: `Hi, You have a new score: ${req.body.score}`,
    html: output,
  }

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      logger.log('error', `Failed in sending email ${error}`)

      res
        .status(500)
        .json({
          message: 'Error in sending score',
        })
        .end()
    } else {
      logger.log('info', 'email send')

      res
        .status(201)
        .json({
          message: 'Email has been sent',
        })
        .end()
    }
  })
})

module.exports = route
