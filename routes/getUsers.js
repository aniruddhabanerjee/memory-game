const express = require('express')
const { Users } = require('../models')
const logger = require('../log/logger')

const route = express.Router()

route.get('/', (req, res) => {
  Users.findAll()
    .then((user) => {
      if (user.length > 0) {
        res.status(200).json(user).end()
      } else {
        logger.log('info', `Users not found!!!! :(`)
        console.log(err)
        res
          .status(404)
          .json({
            message: `Users not found!!!! :(`,
          })
          .end()
      }
    })
    .catch((err) => {
      logger.log('error', err)
      console.log(err)
      res
        .status(500)
        .json({
          message: 'Error in Fetching Users',
        })
        .end()
    })
})

route.post('/', (req, res) => {
  Users.create({
    name: req.body.name,
    email: req.body.email,
    score: req.body.score,
  })
    .then((user) => {
      res.status(201).json(user).end()
    })
    .catch((err) => {
      logger.log('error', err)

      res
        .status(500)
        .json({
          message: 'Error in posting Users score',
        })
        .end()
    })
})

route.get('/score', (req, res) => {
  Users.findAll({ order: [['score', 'ASC']], limit: 5 })
    .then((user) => {
      if (user.length > 0) {
        res.status(200).json(user).end()
      } else {
        logger.log('info', `Users not found!!!! :(`)

        res
          .status(404)
          .json({
            message: `Users not found!!!! :(`,
          })
          .end()
      }
    })
    .catch((err) => {
      logger.log('error', err)

      res
        .status(500)
        .json({
          message: 'Error in Fetching Users',
        })
        .end()
    })
})

module.exports = route
