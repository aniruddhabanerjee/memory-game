const gameContainer = document.getElementById('game')
const scoreElement = document.getElementById('current-score')
const highScoreElement = document.getElementById('highScore')
const start = document.getElementById('start')
const reset = document.getElementById('reset')
const cancel = document.getElementById('cancel')
const confirmBox = document.getElementById('confirm')
const confirmation = document.getElementById('confirmation')
const confirmOverlay = document.getElementById('confirm-overlay')
const playAgainOverlay = document.getElementById('playagain-overlay')
const playAgain = document.getElementById('playagain')
const cancelPlay = document.getElementById('cancel-play')
const form = document.getElementById('form')
const username = document.getElementById('name')
const email = document.getElementById('email')
const home = document.getElementById('home')
home.addEventListener('click', () => {
  location.href = 'index.html'
  resetGamePlay()
})
highScoreElement.innerText = localStorage.getItem('highScore') || 0

const numArray = Array(9)
  .fill()
  .map((element, index) => index + 1)

const arr = [...numArray, ...numArray]

let shuffledArr = shuffle(arr)

let isFlipped = false
let firstCard, secondCard
let waitForCard = false
let gamePlay = false
let score = 0
let gameOver = false

let highScore = Number(localStorage.getItem('highScore')) || 0

reset.addEventListener('click', () => {
  confirmOverlay.style.display = 'block'
})

cancel.addEventListener('click', () => {
  confirmOverlay.style.display = 'none'
})

cancelPlay.addEventListener('click', () => {
  playAgainOverlay.style.display = 'none'
})

playAgain.addEventListener('click', () => {
  playAgainOverlay.style.display = 'none'
  resetGamePlay()
})

confirmation.addEventListener('click', () => {
  confirmOverlay.style.display = 'none'
  resetGamePlay()
})

createDivForColors(shuffledArr)

function createDivForColors(imgArr) {
  for (let img of imgArr) {
    const newDiv = document.createElement('div')

    newDiv.classList.add(img)
    newDiv.innerHTML = `
    <img class='front' src="./gifs/${img}.gif" alt='front' />
    <img class='back' src="https://images.unsplash.com/32/Mc8kW4x9Q3aRR3RkP5Im_IMG_4417.jpg?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80" alt='back' />
    
  `
    newDiv.addEventListener('click', handleCardClick)

    gameContainer.append(newDiv)
  }
}

function handleCardClick() {
  if (waitForCard) return

  if (this.classList.contains('flip')) return

  if (firstCard) {
    reset.style.display = 'block'
  }

  const imgId = this.getAttribute('class')

  this.setAttribute('data', imgId)

  this.classList.add('flip')

  if (!isFlipped) {
    isFlipped = true
    firstCard = this
  } else {
    isFlipped = false
    secondCard = this

    if (firstCard.getAttribute('data') === secondCard.getAttribute('data')) {
      removeClickHandler()
    } else {
      waitForCard = true
      setTimeout(() => {
        removeFlip()

        waitForCard = false
      }, 1000)
    }
  }
  updateScore()
  updateHighScore()
}

function resetGamePlay() {
  const gameDiv = document.querySelectorAll('#game div')

  gameDiv.forEach((element) => {
    element.classList.remove('flip')
    element.addEventListener('click', handleCardClick)
  })
  score = 0
  scoreElement.innerText = score
  gameContainer.innerHTML = ''
  shuffledArr = shuffle(arr)
  form.style.display = 'flex'
  createDivForColors(shuffledArr)
}

function shuffle(array) {
  let counter = array.length

  while (counter > 0) {
    let index = Math.floor(Math.random() * counter)

    counter--

    let temp = array[counter]
    array[counter] = array[index]
    array[index] = temp
  }

  return array
}

function removeClickHandler() {
  firstCard.removeEventListener('click', handleCardClick)
  secondCard.removeEventListener('click', handleCardClick)
}

function removeFlip() {
  firstCard.classList.remove('flip')
  secondCard.classList.remove('flip')
}

function updateScore() {
  score++
  scoreElement.innerText = score
}

function updateHighScore() {
  const gameDiv = document.querySelectorAll('#game div')
  const game = []

  gameDiv.forEach((element) => {
    if (element.classList.contains('flip')) {
      game.push(true)
    } else {
      game.push(false)
    }
  })

  gameOver = game.every((element) => element == true)

  if (gameOver) {
    if (highScore == 0) {
      localStorage.setItem('highScore', score)
      highScoreElement.innerText = localStorage.getItem('highScore')
    } else {
      if (score < highScore) {
        localStorage.setItem('highScore', score)
        highScoreElement.innerText = localStorage.getItem('highScore')
      }
    }
    playAgainOverlay.style.display = 'block'
    form.addEventListener('submit', (e) => {
      e.preventDefault()
      const user = {
        name: username.value,
        email: email.value,
        score: score,
      }
      postUserScore('/user', user)
        .then((data) => {
          console.log(data)
        })
        .catch((err) => {
          console.log(err)
          console.log('error in uploading data')
        })
      sendEmail('/send', user)
        .then((data) => {
          console.log(data)
        })
        .catch((err) => {
          console.log(err)
          console.log('error in uploading data')
        })
      form.style.display = 'none'
    })
  }
}

function postUserScore(url, data) {
  return new Promise((resolve, reject) => {
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then((res) => resolve(res.json()))
      .catch((err) => {
        console.log(err)
        reject(err)
      })
  })
}
function sendEmail(url, data) {
  return new Promise((resolve, reject) => {
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    })
      .then((res) => resolve(res.json()))
      .catch((err) => {
        console.log(err)
        reject(err)
      })
  })
}
