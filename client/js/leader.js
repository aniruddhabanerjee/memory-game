const leaderBoard = document.getElementById('leaderBoard')
const table = document.getElementById('table')
fetch('/user/score')
  .then((res) => res.json())
  .then((data) => {
    createLeaderBoard(data)
  })
  .catch((err) => console.log(err))

function createLeaderBoard(data) {
  data.forEach((element) => {
    const newRow = document.createElement('tr')
    const col1 = document.createElement('td')
    const col2 = document.createElement('td')
    const col3 = document.createElement('td')
    const col4 = document.createElement('td')
    col1.innerText = element.name
    col2.innerText = element.email
    col3.innerText = element.score
    col4.innerText = element.createdAt.slice(0, 10)
    newRow.appendChild(col1)
    newRow.appendChild(col2)
    newRow.appendChild(col3)
    newRow.appendChild(col4)
    table.appendChild(newRow)
  })
}
