const Sequelize = require('sequelize')
const { db_database, db_host, db_password, db_username } = require('../config')
const db = {}

let sequelize = new Sequelize(db_database, db_username, db_password, {
  host: db_host,
  dialect: 'mysql',
})

db.sequelize = sequelize
db.Sequelize = Sequelize

db.Users = require('./Users')(sequelize, Sequelize)

module.exports = db
